*Still under development*

Scrum test
==========

This is a simple questionaire with Scrum related questions. It is almost mutiple choice: you have to order the answers from best to worst.

After finishing the questionaire, you will be presented with your matching score to the Scrum leaders we have asked to also order the answers.

Adding your benchmark
=====================
To add your own benchmark, do the following:

1. Order all the answers for each question
2. Watch the movie "The Net"
3. Click on the Pi sign in the bottom right corner (while thinking _ooh, now I get it_)
4. Click on the "Create Benchmark" button
5. Copy the code block and paste it into `src/main/js/script.js` in the `benchmarks` object under your name/id.

Development
===========
_Requires_ Python and a browser

1. Open a terminal and run `src/host.sh`
2. Open `http://localhost:4000/` in your favorite browser
3. Choose either test of main from the homepage

Backlog
=======

*   Allow the user to drill down on their difference with the benchmarks
*   The number of answers now influences the weight of the question, it should not.
