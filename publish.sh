#!/bin/bash
cd "`dirname "$0"`"
TMPPATH="/tmp/$$"
rm -rf "$TMPPATH"
mkdir "$TMPPATH"
cp -r src/scrumtest/* "$TMPPATH"
git checkout gh-pages
cp -r "$TMPPATH/"* .
echo "Done, check and commit if everything is correct"
