
var questions = {
    "why_ask_commitment": {
        value: "Why ask for commitment from the team?",
        answers: [
            "To make sure you can call them on it when they fail",
            "To get across how important the task at hand is",
            "To get them to seek out autonomy during the sprint",
            "To be able to detect which developer is likely to be fired next"]
    },
    "why_scrum_master": {
        value: "Why do you have a scrum master?",
        answers: [
            "To lead the team",
            "To keep all formal things going",
            "To keep meetings short",
            "To make sure the burn-down chart actually goes down",
            "To motivate the team from a developer's perspective"]
    },
    "why_have_a_backlog": {
        value: "Why should you have a backlog?",
        answers: [
            "No issue left behind!",
            "To know when you are done with the product",
            "To keep the team from getting overwhelmed",
            "To allow the Product owner to add requests during a sprint",
            "To formalize communication between the team and the outside world"]
    } /*
    "why_have_a_standup": {
        value: "Why should you have a daily standup?",
        answers: [
            "To allow the team to communicate problems to each other",
            "To get developers to confess on what you have been doing",
            "To keep an eye on progress",
            "To allow people to discuss their actions and plans",
            "To give the Product Owner to stear on a daily basis to minimize waste"]
    } */
};


var benchmarks = {
    "Bram": {"why_ask_commitment": [2, 1, 0, 3], "why_scrum_master": [1, 3, 2, 4, 0], "why_have_a_backlog": [2, 0, 4, 3, 1]},
    "Jan": {"why_ask_commitment": [2, 1, 0, 3], "why_scrum_master": [4, 2, 3, 0, 1], "why_have_a_backlog": [4, 2, 0, 3, 1]}
};




jQuery.extend({
    compare: function (arrayA, arrayB) {
        var i;
        if (arrayA.length !== arrayB.length) {
            return false;
        }
        for (i = 0; i < arrayA.length; i += 1) {
            if (arrayA[i] !== arrayB[i]) {
                return false;
            }
        }
        return true;
    }
});

function moveIn(elements, at, after) {
    var e = elements.splice(at, 1);
    elements.splice(after, 0, e[0]);
}


//Recursively mutate the elements till it matches seek, then return minimum number of mutations needed
function mutate(seek, elements, nActions) {
    if ($.compare(seek, elements)) {
        return nActions;
    }

    if (nActions === elements.length) {
        return nActions + 1;
    }

    var minNActions = elements.length, ai, bi, e, v; //set minimum to maximum return value of mutate
    for (ai = 0; ai < elements.length - 1; ai += 1) {
        for (bi = ai + 1; bi < elements.length; bi += 1) {
            e = elements.slice();
            moveIn(e, ai, bi);
            v = mutate(seek, e, nActions + 1);
            if (v < minNActions) {
                minNActions = v;
            }
            if (minNActions <= nActions + 1) { //Next swap is already an answer. Can't get any better than that
                break;
            }
        }
        if (minNActions <= nActions + 1) { //Next swap is already an answer. Can't get any better than that
            break;
        }
    }
    return minNActions;
}

function differenceBetween(benchmark, answers) {
    return { max: benchmark.length, diff: mutate(benchmark, answers, 0)};
}

function displayAnswers(scores) {
    var results = $('#results');
    results.html('');
    $.each(scores, function(benchmark, score) {
        results.append($('<p class="result">').text(Math.floor((score.max - score.diff) * 100 / score.max) + "% match with " + benchmark + "."));
    })
    $("#resultsbox").show("bounce");
}


function createBenchmark(evt) {
    var results = $('#results'), newBenchmark = {}, answers, questionId;
    results.html('');
    $('ul').each(function (questionIndex, questionElement) {
        questionId = questionElement.getAttribute("id");
        answers = $("li", questionElement).map(function (index, element) { return Number(element.getAttribute("answeridx")); });
        newBenchmark[questionId] = answers.get();
    });
    results.append($("<pre />").text(JSON.stringify(newBenchmark)));
    $("#resultsbox").show("bounce");
}

function calculateScores(event) {
    //Collect answer id's in order
    var scores = {}, benchmarkName, questionId;
    for (benchmarkName in benchmarks) {
        scores[benchmarkName] = {max: 0, diff: 0};
    }
    $('ul').each(function(questionIndex, questionElement) {
        questionId = questionElement.getAttribute("id");
        answers = $("li", questionElement).map(function(index, element) { return Number(element.getAttribute("answeridx"));});
        $.each(benchmarks, function(benchmarkName, benchmark) {
            var score = differenceBetween(benchmark[questionId], answers.get());
            scores[benchmarkName].max += score.max;
            scores[benchmarkName].diff += score.diff;
        });
    });
    displayAnswers(scores);
}

