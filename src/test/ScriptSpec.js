describe("ScrumTest", function() {

  it("every benchmark should mention all questions", function() {
    $.each(benchmarks, function(bmid, benchmark) {
        $.each(questions, function(questionId, question) {
            expect(benchmark[questionId]).toBeDefined();
        });
    });
  });
  it("every benchmark should have as many answers as the questions do", function() {
    $.each(benchmarks, function(bmid, benchmark) {
        $.each(questions, function(questionId, question) {
            expect(benchmark[questionId].length).toEqual(question.answers.length);
        });
    });
  });

  it("More difference, will result in a larger difference", function() {
    expect(differenceBetween([1,2], [1,2]).diff).toBeLessThan(differenceBetween([1,2], [2,1]).diff);  
    expect(differenceBetween([1,2,3], [1,2,3]).diff).toBeLessThan(differenceBetween([1,2,3], [1,3,2]).diff);  
  });


  it("The index of the answer should not influence the difference", function() {
    expect(differenceBetween([1,2], [1,2]).diff).toEqual(differenceBetween([2,3], [2,3]).diff); 
    expect(differenceBetween([1,2], [2,1]).diff).toEqual(differenceBetween([2,3], [3,2]).diff); 
    expect(differenceBetween([1,2], [2,1]).diff).toEqual(differenceBetween([2,8], [8,2]).diff);
     
    expect(differenceBetween([1,2,3], [1,3,2]).diff).toEqual(differenceBetween([1,2,3], [2,1,3]).diff);  
  });

  it("An offset in questions, should not create a large difference", function() {
    //Single change in sequence
    expect(differenceBetween([1,2], [2,1]).diff).toEqual(differenceBetween([1,2,3,4], [1,4,2,3]).diff);  
  });
  
  it("Comparison should work for all arrays", function() {
    //Single change in sequence
    expect($.compare([1,2,3,4], [1,2,3,4])).toBe(true);
  });
  
  it("Moving should move the elements", function() {
    //Single change in sequence
    var elements = [1,2,3,4];
    expect($.compare(elements, [1,2,3,4])).toBe(true);
    moveIn(elements, 0, 1);
    expect($.compare(elements, [2,1,3,4])).toBe(true);
  });

  it("difference can never be larger than the length of the choices", function() {
    var length = 5;
    var elements = $.map(new Array(length), function(val, idx) { return idx; });
    var randomized = elements.slice();
    for(var t = 0; t < 10; ++t) {
        //Randomize elements
        for(var i = 0; i < randomized.length; ++i) {
            randomized.sort(function() {return 0.5 - Math.random()});
        }
        var diff = differenceBetween(elements, randomized).diff;
        expect(diff).toBeLessThan(elements.length);
    }
  });

});
